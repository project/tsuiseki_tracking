<?php
/**
 * @file
 * Please adjust the settings here and rename this file to config.inc.php.
 *
 * @author Jens Jahnke <jan0sch@gmx.net>
 */

/**
 * Here you can define your tsuiseki tracking key.
 * This define is intended to speed things up. If you leave it empty
 * each view/click will cost you an additional database query for looking
 * up the key (variable_get).
 */
define('TSUISEKI_TRACKER_KEY', NULL);

/**
 * Definiert die CSS-Klasse für einen ausgehenden Link.
 */
define('TSUISEKI_TRACKER_CSS_CLASS', NULL);

/**
 * Enter the value in seconds here after which data stored into the
 * $_SESSION variable shall be considered invalid.
 */
define('TSUISEKI_TRACKING_SESSION_TIMEOUT', 1800);
