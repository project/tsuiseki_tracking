<?php
/**
 * @file
 * This file contains the administration function for tsuiseki tracking module.
 *
 * @author Jens Jahnke <jan0sch@gmx.net>
 */

/**
 * Generates the administration formular for the module.
 *
 * @param array $form_state
 * @return array
 */
function tsuiseki_tracking_admin($form_state) {
  $form = array();

  drupal_set_message(t('Always remember that the values you enter here for the tracking key and the css class will be overridden by settings from the configuration file (<em>config.inc.php</em>)!'));

  $form['tsuiseki_auth_settings'] = array(
    '#title' => t('Authentification settings'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $msg = '';
  $key = tsuiseki_tracking_get_key();
  $v_key = (string)trim(variable_get('tsuiseki_tracking_key', NULL));
  if (!empty($key) && empty($v_key)) {
    $msg = t('<div class="messages notice">A key is defined in you configuration file.</div>');
  }
  $form['tsuiseki_auth_settings']['tsuiseki_tracking_key'] = array(
    '#title' => t('Tsuiseki tracking key'),
    '#description' => t('Enter your tracking key here. Without it nothing can be logged. <strong>You should leave this empty and define the key directly in the <em>config.inc.php</em>.</strong> <p>You can get your tracking key at !link.</p>', array('!link' => l('tsuiseki.com', 'http://www.tsuiseki.com/', array('attributes' => array('target' => '_blank'))))),
    '#type' => 'textfield',
    '#max_length' => 64,
    '#default_value' => (string)trim(variable_get('tsuiseki_tracking_key', NULL)),
    '#suffix' => $msg,
  );

  $form['tsuiseki_settings'] = array(
    '#title' => t('Tracking settings'),
    '#type' => 'fieldset',
  );
  $form['tsuiseki_settings']['tsuiseki_tracking_css_class'] = array(
    '#title' => t('CSS class for outgoing links'),
    '#description' => t('Define a CSS class for outgoing links. This is used for click tracking via ajax. You can leave this empty and define the class directly in the <em>config.inc.php</em>. If you define both parameters the entry from the config file is preferred. Ensure that the id of the regarding elements is defined as it is used as the click type.'),
    '#type' => 'textfield',
    '#max_length' => 64,
    '#default_value' => (string)trim(variable_get('tsuiseki_tracking_css_class', 'a.tsuiseki-link')),
  );
  $form['tsuiseki_settings']['tsuiseki_tracking_exclude_uris'] = array(
    '#title' => t('Exclude the following paths'),
    '#description' => t('The paths you list here (one per line) will be excluded from the tracking process. Use values like <em>foo/*</em> to exclude any path beginning with <em>foo/</em>.'),
    '#type' => 'textarea',
    '#default_value' => (string)variable_get('tsuiseki_tracking_exclude_uris', TSUISEKI_TRACKING_DEFAULT_EXCLUSIONS),
  );

  $form['tsuiseki_advanced_settings'] = array(
    '#title' => t('Advanced settings'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['tsuiseki_advanced_settings']['tsuiseki_tracking_disable'] = array(
    '#title' => t('Disable tracking'),
    '#description' => t('<strong>Attention!</strong> You can disable the whole tracking by enabling this option.'),
    '#type' => 'checkbox',
    '#default_value' => (int)variable_get('tsuiseki_tracking_disable', 0),
  );

  return system_settings_form($form);
} // function tsuiseki_tracking_admin